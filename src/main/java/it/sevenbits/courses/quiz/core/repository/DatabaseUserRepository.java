package it.sevenbits.courses.quiz.core.repository;
import it.sevenbits.courses.quiz.core.model.User;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.HashMap;

/**
 * Database implementation of IUserRepository
 */
public class DatabaseUserRepository implements IUserRepository {
    private final JdbcOperations jdbcOperations;
    /**
     * Constructor
     * @param jdbcOperations JdbcOperations instance for our db
     */
    public DatabaseUserRepository(final JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public User addUser(final String email, final String password) {
        UUID uuid = UUID.randomUUID();
        int rows = jdbcOperations.update(
                "INSERT INTO users (userId, email, password, enabled) VALUES (?, ?, ?, ?);",
                uuid.toString(), email, password, true
        );
        jdbcOperations.update("INSERT INTO user_roles (email, role) VALUES (?, ?);",
                email, "USER"
                );
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        return new User(uuid.toString(), email, password, roles);
    }

    @Override
    public List<User> getAllUsers() {
        List<Map<String, Object>> rawUsers;
        try {
            rawUsers = jdbcOperations.query(
                    "SELECT userId, email, password FROM users;",
                    (resultSet, i) -> {
                        Map<String, Object> user = new HashMap<>();
                        String userId = resultSet.getString("userId");
                        String email = resultSet.getString("email");
                        String password = resultSet.getString("password");
                        user.put("userId", userId);
                        user.put("email", email);
                        user.put("password", password);
                        return user;
                    }
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
        List<User> users = new ArrayList<>();
        for (Map<String, Object> rawUser : rawUsers) {
            List<String> roles = new ArrayList<>();
            String email = String.valueOf(rawUser.get("email"));
            jdbcOperations.query(
                    "SELECT email, role FROM user_roles" +
                            " WHERE email = ?",
                    resultSet -> {
                        String role = resultSet.getString("role");
                        roles.add(role);
                    },
                    email
            );
            String userId = String.valueOf(rawUser.get("userId"));
            String password = String.valueOf(rawUser.get("password"));
            users.add(new User(userId, email, password, roles));
        }
        return users;
    }

    @Override
    public User getUserByEmail(final String email) {
        Map<String, Object> rawUser;
        try {
            rawUser = jdbcOperations.queryForMap(
                    "SELECT userId, email, password FROM users u" +
                            " WHERE u.enabled = true AND u.email = ?",
                    email
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }


        List<String> roles = new ArrayList<>();
        jdbcOperations.query(
                "SELECT email, role FROM user_roles" +
                        " WHERE email = ?",
                resultSet -> {
                    String role = resultSet.getString("role");
                    roles.add(role);
                },
                email
        );
        String userId = String.valueOf(rawUser.get("userId"));
        String password = String.valueOf(rawUser.get("password"));
        return new User(userId, email, password, roles);
    }

    @Override
    public User getUserById(final String id) {
        Map<String, Object> rawUser;
        try {
            rawUser = jdbcOperations.queryForMap(
                    "SELECT userId, email, password FROM users u" +
                            " WHERE u.enabled = true AND u.userId = ?",
                    id
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }

        String email = String.valueOf(rawUser.get("email"));
        List<String> roles = new ArrayList<>();
        jdbcOperations.query(
                "SELECT email, role FROM user_roles" +
                        " WHERE email = ?",
                resultSet -> {
                    String role = resultSet.getString("role");
                    roles.add(role);
                },
                email
        );

        String password = String.valueOf(rawUser.get("password"));
        return new User(id, email, password, roles);
    }
}
