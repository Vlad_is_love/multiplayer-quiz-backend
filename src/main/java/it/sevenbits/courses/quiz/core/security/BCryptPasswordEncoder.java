package it.sevenbits.courses.quiz.core.security;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Implementation of PasswordEncoder via BCrypt library
 */
public class BCryptPasswordEncoder implements PasswordEncoder {

    @Override
    public boolean matches(final String plainPassword, final String hashedPassword) {
        return BCrypt.checkpw(plainPassword.toString(), hashedPassword);
    }
}