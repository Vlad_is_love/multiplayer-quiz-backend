package it.sevenbits.courses.quiz.core.config;

import it.sevenbits.courses.quiz.core.security.BCryptPasswordEncoder;
import it.sevenbits.courses.quiz.core.security.PasswordEncoder;
import it.sevenbits.courses.quiz.web.security.JsonWebTokenService;
import it.sevenbits.courses.quiz.web.security.JwtSettings;
import it.sevenbits.courses.quiz.web.security.JwtTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * Class for security configuration
 */
@Configuration
public class SecurityConfig {
    /**
     * method for getting implementation of PasswordEncoder
     * @return PasswordEncoder implementation
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    /**
     * method for getting implementation of JwtTokenService
     * @param settings jwt settings
     * @return JwtTokenService implementation
     */
    @Bean
    public JwtTokenService jwtTokenService(final JwtSettings settings) {
        return new JsonWebTokenService(settings);
    }

}

