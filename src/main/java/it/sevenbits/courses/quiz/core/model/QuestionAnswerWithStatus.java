package it.sevenbits.courses.quiz.core.model;

/**
 * Answer for the question with status if it's correct or not
 */
public class QuestionAnswerWithStatus extends QuestionAnswer {
    private final boolean correct;
    /**
     * Constructor
     * @param answerId id of the answer
     * @param answerText text of the answer
     * @param correct if answer correct or not
     */
    public QuestionAnswerWithStatus(final String answerId, final String answerText, final boolean correct) {
        super(answerId, answerText);
        this.correct = correct;
    }
    /**
     * method that returns if answer is correct or not
     * @return correct or not
     */
    public boolean isCorrect() {
        return correct;
    }
}
