package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.Room;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
/**
 * Database implementation for IRoomRepository
 */
@Repository
public class DatabaseRoomRepository implements IRoomRepository {
    private final JdbcOperations jdbcOperations;
    /**
     * Constructor
     * @param jdbcOperations JdbcOperations instance for our db
     */
    public DatabaseRoomRepository(final JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }
    @Override
    public Room getRoomById(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT roomId, roomName, playersIds, ownerId FROM room WHERE roomId = ?;",
                (resultSet, i) -> {
                    return getRoomFromResultSet(resultSet);
                },
                roomId);
    }

    private Room getRoomFromResultSet(final ResultSet resultSet) throws SQLException {
        String id = resultSet.getString("roomId");
        String roomName = resultSet.getString("roomName");
        String ownerId = resultSet.getString("ownerId");
        List<String> playersIds =  Arrays.asList((String[]) resultSet.getArray("playersIds").getArray());
        List<Map<String, String>> players = new ArrayList<>();
        for (String elem : playersIds) {
            Map<String, String> player = new HashMap<>();
            player.put("playerId", elem);
            players.add(player);
        }
        return new Room(id, roomName, players, ownerId);
    }

    @Override
    public List<Room> getRooms() {
        return jdbcOperations.query(
                "SELECT roomId, roomName, playersIds, ownerId FROM room;",
                (resultSet, i) -> getRoomFromResultSet(resultSet));
    }

    @Override
    public Room createRoom(final Room room) {
        String roomId = room.getRoomId();
        String roomName = room.getRoomName();
        String ownerId = room.getOwnerId();
        List<Map<String, String>> playersId = room.getPlayers();
        List<String> tempList = new ArrayList<>();
        for (Map<String, String> player : playersId) {
            tempList.add(player.get("playerId"));
        }
        String[] sqlArrayOfPlayersIds = tempList.toArray(new String[tempList.size()]);
        int rows = jdbcOperations.update(
                "INSERT INTO room (roomId, roomName, playersIds, ownerId) VALUES (?, ?, ?, ?);",
                roomId, roomName, sqlArrayOfPlayersIds, ownerId
        );
        return new Room(roomId, roomName, playersId, ownerId);
    }

    @Override
    public Room updateRoom(final Room room) {
        String roomId = room.getRoomId();
        String roomName = room.getRoomName();
        String ownerId = room.getOwnerId();
        List<Map<String, String>> playersId = room.getPlayers();
        List<String> tempList = new ArrayList<>();
        for (Map<String, String> player : playersId) {
            tempList.add(player.get("playerId"));
        }
        String[] sqlArrayOfPlayersIds = tempList.toArray(new String[tempList.size()]);
        int rows = jdbcOperations.update(
                "UPDATE room SET roomName = ?, playersIds = ?, ownerId = ? WHERE roomId = ?;",
                roomName, sqlArrayOfPlayersIds, ownerId, roomId
        );
        return new Room(roomId, roomName, playersId, ownerId);
    }
}
