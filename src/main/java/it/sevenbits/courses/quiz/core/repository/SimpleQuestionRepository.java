package it.sevenbits.courses.quiz.core.repository;

import com.fasterxml.jackson.databind.MappingIterator;
import it.sevenbits.courses.quiz.core.model.QuestionAnswer;
import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Simple realisation of IQuestionRepository
 */
@Repository
public class SimpleQuestionRepository implements IQuestionRepository {
    private final List<QuestionWithOptionsAndCorrectAnswer> questions;

    /**
     * Constructor
     * @throws IOException standard java exception
     */
    public SimpleQuestionRepository() throws IOException {
        questions = new ArrayList<>();
        List<Map<String, String>> response = new LinkedList<>();
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        MappingIterator<Map<String, String>> iterator = mapper.reader(Map.class)
                .with(schema)
                .readValues(/*new File("src/main/resources/config/questions.csv")*/
                SimpleQuestionRepository.class.getResource("/config/questions.csv"));
        while (iterator.hasNext()) {
            response.add(iterator.nextValue());
        }
        for (Map<String, String> map : response) {
            List<QuestionAnswer> answers = new ArrayList<>();
            answers.add(new QuestionAnswer(map.get("option1")));
            answers.add(new QuestionAnswer(map.get("option2")));
            answers.add(new QuestionAnswer(map.get("option3")));
            answers.add(new QuestionAnswer(map.get("option4")));
            String correctAnswerString = map.get("correctAnswer");
            QuestionAnswer correctAnswer = null;
            for (QuestionAnswer answerOption : answers) {
                if (answerOption.getAnswerText().equals(correctAnswerString)) {
                    correctAnswer = new QuestionAnswer(answerOption.getAnswerId(), correctAnswerString);
                }
            }
            questions.add(new QuestionWithOptionsAndCorrectAnswer(map.get("question"), answers, correctAnswer));
        }
    }

    @Override
    public QuestionWithOptionsAndCorrectAnswer getFullQuestion(final String questionId) {
        for (QuestionWithOptionsAndCorrectAnswer question : questions) {
            if (question.getQuestionId().equals(questionId)) {
                return question;
            }
        }
        return null;
    }
    @Override
    public List<String> getQuestionsIds() {
        List<String> questionIds = new ArrayList<>();
        for (QuestionWithOptionsAndCorrectAnswer question : questions) {
            questionIds.add(question.getQuestionId());
        }
        return questionIds;
    }
}
