package it.sevenbits.courses.quiz.core.exception;
/**
 * exception that occurs in game logic errors
 */
public class GameRuleException extends Exception {

    private final GameRuleErrorCode gameRuleErrorCode;
    /**
     * Constructor
     * @param gameRuleErrorCode error code
     */
    public GameRuleException(final GameRuleErrorCode gameRuleErrorCode) {
        super(gameRuleErrorCode.getErrorString());
        this.gameRuleErrorCode = gameRuleErrorCode;
    }

    public GameRuleErrorCode getErrorCode() {
        return gameRuleErrorCode;
    }
}
