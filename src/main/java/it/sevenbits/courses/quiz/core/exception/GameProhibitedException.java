package it.sevenbits.courses.quiz.core.exception;

/**
 * exception that occurs when something is prohibited for player
 */
public class GameProhibitedException extends Exception {

    private final GameRuleErrorCode gameRuleErrorCode;
    /**
     * Constructor
     * @param gameRuleErrorCode error code
     */
    public GameProhibitedException(final GameRuleErrorCode gameRuleErrorCode) {
        super(gameRuleErrorCode.getErrorString());
        this.gameRuleErrorCode = gameRuleErrorCode;
    }

    public GameRuleErrorCode getErrorCode() {
        return gameRuleErrorCode;
    }
}