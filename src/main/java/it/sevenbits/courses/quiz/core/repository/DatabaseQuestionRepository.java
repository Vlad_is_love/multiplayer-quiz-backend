package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.QuestionAnswer;
import it.sevenbits.courses.quiz.core.model.QuestionAnswerWithStatus;
import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
 * Database implementation for IQuestionRepository
 */
@Repository
public class DatabaseQuestionRepository implements IQuestionRepository {
    private final JdbcOperations jdbcOperations;
    /**
     * Constructor
     * @param jdbcOperations JdbcOperations instance for our db
     */
    public DatabaseQuestionRepository(final JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public QuestionWithOptionsAndCorrectAnswer getFullQuestion(final String questionId) {
        String questionText = jdbcOperations.queryForObject(
                "SELECT text FROM question WHERE questionId = ?;",
                (resultSet, i) -> {
                    return resultSet.getString("text");
                },
                questionId);
        List<QuestionAnswerWithStatus> questionAnswersWithStatus = jdbcOperations.query(
                "SELECT answerId, text, correct FROM answer WHERE questionId = ?;",
                (resultSet, i) -> {
                    String answerId = resultSet.getString("answerId");
                    String text = resultSet.getString("text");
                    boolean correct = resultSet.getBoolean("correct");
                    return new QuestionAnswerWithStatus(answerId, text, correct);
                },
                questionId);
        List<QuestionAnswer> answers = new ArrayList<>();
        QuestionAnswer correctAnswer = null;
        for (QuestionAnswerWithStatus question : questionAnswersWithStatus) {
            if (question.isCorrect()) {
                correctAnswer = new QuestionAnswer(question.getAnswerId(), question.getAnswerText());
            }
            answers.add(new QuestionAnswer(question.getAnswerId(), question.getAnswerText()));
        }
        QuestionWithOptionsAndCorrectAnswer question = new QuestionWithOptionsAndCorrectAnswer(
          questionId, questionText, answers, correctAnswer
        );
        return question;
    }

    @Override
    public List<String> getQuestionsIds() {
        return jdbcOperations.query(
                "SELECT questionId FROM question;",
                (resultSet, i) -> {
                    return resultSet.getString("questionId");
                });
    }
}
