package it.sevenbits.courses.quiz.core.model;

import java.util.List;

/**
 * question with options and correct answer model
 */
public class QuestionWithOptionsAndCorrectAnswer extends QuestionWithOptions {
    private final QuestionAnswer correctAnswer;

    /**
     * Constructor
     * @param questionText text of the question
     * @param answersList list of the answers
     * @param correctAnswer correct answer
     */
    public QuestionWithOptionsAndCorrectAnswer(final String questionText, final List<QuestionAnswer> answersList,
                                               final QuestionAnswer correctAnswer) {
        super(questionText, answersList);
        this.correctAnswer = correctAnswer;
    }
    /**
     * Constructor for creating question from db
     * @param questionId if of the question
     * @param questionText text of the question
     * @param answersList list of the answers
     * @param correctAnswer correct answer
     */
    public QuestionWithOptionsAndCorrectAnswer(final String questionId, final String questionText, final List<QuestionAnswer> answersList,
                                               final QuestionAnswer correctAnswer) {
        super(questionId, questionText, answersList);
        this.correctAnswer = correctAnswer;
    }

    public QuestionAnswer getCorrectAnswer() {
        return correctAnswer;
    }
}
