package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;

import java.util.List;
/**
 * Interface for game repository
 */
public interface IGameRepository {
    /**
     * method for getting ids of questions of particular room
     * @param roomId id of the room
     * @return ids of questions
     */
    List<String> getQuestionIds(String roomId);
    /**
     * method for setting ids of questions for particular room
     * @param roomId id of the room
     * @param ids ids of questions
     */
    void setQuestionIds(String roomId, List<String> ids);
    /**
     * method for setting game state for particular room
     * @param roomId id of the room
     */
    void setGameStarted(String roomId);
    /**
     * method for setting current question id for particular room
     * @param roomId id of the room
     * @param questionId id of question
     */
    void setCurrentQuestionId(String roomId, String questionId);
    /**
     * method for setting current question for particular room
     * @param roomId id of the room
     * @param question current question
     */
    void setCurrentQuestion(String roomId, QuestionWithOptionsAndCorrectAnswer question);
    /**
     * method for getting current question of the for particular room
     * @param roomId id of the room
     * @return current question
     */
    QuestionWithOptionsAndCorrectAnswer getCurrentQuestion(String roomId);
    /**
     * method for getting number of answered questions of particular room
     * @param roomId id of the room
     * @return number of answered questions
     */
    Integer getNumOfAnsweredQuestions(String roomId);
    /**
     * method for setting number of answered questions for particular room
     * @param roomId id of the room
     * @param num number of answered questions
     */
    void setNumOfAnsweredQuestions(String roomId, Integer num);
    /**
     * method for getting game score of particular room
     * @param roomId id of the room
     * @return game score
     */
    Integer getScore(String roomId);
    /**
     * method for setting game score for particular room
     * @param roomId id of the room
     * @param scoreValue game score
     */
    void setScore(String roomId, Integer scoreValue);
    /**
     * method for checking if game started in particular room
     * @param roomId id of the room
     * @return if game started or not
     */
    Boolean isGameStarted(String roomId);
    /**
     * method for getting current question id in particular room
     * @param roomId id of the room
     * @return current question id
     */
    String getCurrentQuestionId(String roomId);
    /**
     * method for getting question number of particular room
     * @param roomId id of the room
     * @return question number
     */
    Integer getQuestionsNumber(String roomId);
    /**
     * method for creating game for created room
     * @param roomId id of the room
     */
    void createGameForRoom(String roomId);
}
