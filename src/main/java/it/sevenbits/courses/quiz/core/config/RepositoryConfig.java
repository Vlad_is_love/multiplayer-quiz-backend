package it.sevenbits.courses.quiz.core.config;

import it.sevenbits.courses.quiz.core.repository.DatabaseQuestionRepository;
import it.sevenbits.courses.quiz.core.repository.DatabaseRoomRepository;
import it.sevenbits.courses.quiz.core.repository.IQuestionRepository;
import it.sevenbits.courses.quiz.core.repository.IRoomRepository;
import it.sevenbits.courses.quiz.core.repository.IGameRepository;
import it.sevenbits.courses.quiz.core.repository.DatabaseGameRepository;
import it.sevenbits.courses.quiz.core.repository.IUserRepository;
import it.sevenbits.courses.quiz.core.repository.DatabaseUserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
/**
 * Config for getting repositories
 */
@Configuration
public class RepositoryConfig {
    /**
     * method for getting instance of IQuestionRepository
     * @param jdbcOperations - instance of jdbc operations class for db
     * @return instance of IQuestionRepository
     */
    @Bean
    public IQuestionRepository questionRepository(
            @Qualifier("tasksJdbcOperations") final JdbcOperations jdbcOperations) {
        return new DatabaseQuestionRepository(jdbcOperations);
    }
    /**
     * method for getting instance of IRoomRepository
     * @param jdbcOperations - instance of jdbc operations class for db
     * @return instance of IRoomRepository
     */
    @Bean
    public IRoomRepository roomRepository(
            @Qualifier("tasksJdbcOperations") final JdbcOperations jdbcOperations) {
        return new DatabaseRoomRepository(jdbcOperations);
    }
    /**
     * method for getting instance of IGameRepository
     * @param jdbcOperations - instance of jdbc operations class for db
     * @return instance of IGameRepository
     */
    @Bean
    public IGameRepository gameRepository(
            @Qualifier("tasksJdbcOperations") final JdbcOperations jdbcOperations) {
        return new DatabaseGameRepository(jdbcOperations);
    }

    /**
     * method for getting instance of IUserRepository
     * @param jdbcOperations - instance of jdbc operations class for db
     * @return instance of IUserRepository
     */
    @Bean
    public IUserRepository userRepository(
            @Qualifier("tasksJdbcOperations") final JdbcOperations jdbcOperations) {
        return new DatabaseUserRepository(jdbcOperations);
    }
}
