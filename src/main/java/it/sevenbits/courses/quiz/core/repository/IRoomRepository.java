package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.Room;

import java.util.List;
/**
 * Interface for room repository
 */
public interface IRoomRepository {
    /**
     * method for getting room by its id
     * @param roomId id of the room
     * @return room
     */
    Room getRoomById(String roomId);
    /**
     * method for getting all the rooms
     * @return list of rooms
     */
    List<Room> getRooms();
    /**
     * method for creating room in memory
     * @param room which to create
     * @return the same room
     */
    Room createRoom(Room room);
    /**
     * method for updating room state in memory
     * @param room which to update
     * @return the same room
     */
    Room updateRoom(Room room);
}
