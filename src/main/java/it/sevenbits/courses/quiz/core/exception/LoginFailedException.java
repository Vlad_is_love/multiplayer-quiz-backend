package it.sevenbits.courses.quiz.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * Simple realisation of IQuestionRepository
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class LoginFailedException extends RuntimeException {
    /**
     * Constructor
     * @param message exception message
     * @param cause cause
     */
    public LoginFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }
    /**
     * Constructor
     * @param message exception message
     */
    public LoginFailedException(final String message) {
        super(message);
    }

}
