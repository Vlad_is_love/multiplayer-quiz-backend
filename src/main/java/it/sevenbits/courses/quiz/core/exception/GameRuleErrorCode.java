package it.sevenbits.courses.quiz.core.exception;
/**
 * error code for game rule exception
 */
public enum GameRuleErrorCode {
    /**
     * inappropriate answer or wrong question error code
     */
    INAPPROPRIATE_ANSWER_OR_WRONG_QUESTION("You answered with an answer from other" +
            " question or answered not the current question"),
    /**
     * empty pool of questions error code
     */
    EMPTY_POOL_OF_QUESTIONS("Number of questions has been ended"),
    /**
     * room not found error code
     */
    ROOM_NOT_FOUND("Room was not found"),
    /**
     * the player is not in the specific room error code
     */
    PLAYER_NOT_IN_ROOM("The player is not in the specific room"),
    /**
     * only owner of the room can start the game
     */
    PLAYER_CANT_START_THE_GAME("Only owner of the room can start the game");

    private final String errorString;

    /**
     * Constructor
     * @param errorString error string
     */
    GameRuleErrorCode(final String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}

