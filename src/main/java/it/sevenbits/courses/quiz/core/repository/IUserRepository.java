package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.User;

import java.util.List;
/**
 * Interface for user repository
 */
public interface IUserRepository {
    /**
     * method that allow to get all users
     * @return users list
     */
    List<User> getAllUsers();
    /**
     * method that allows to get user by email
     * @param email user email
     * @return user
     */
    User getUserByEmail(String email);
    /**
     * method that allows to get user by id
     * @param id user id
     * @return user
     */
    User getUserById(String id);
    /**
     * method that allows to add new user
     * @param email user email
     * @param password user password
     * @return user
     */
    User addUser(final String email, final String password);
}
