package it.sevenbits.courses.quiz.core.model;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * question with options model
 */
public class QuestionWithOptions {
    private final String questionId;
    private final String questionText;
    @Autowired
    private final List<QuestionAnswer> answersList;

    /**
     * Constructor
     * @param questionText text of the question
     * @param answersList list of the answers
     */
    public QuestionWithOptions(final String questionText, final List<QuestionAnswer> answersList) {
        this(UUID.randomUUID().toString(), questionText, answersList);
    }
    /**
     * Constructor for existing id
     * @param questionId id of the question
     * @param questionText text of the question
     * @param answersList list of the answers
     */
    public QuestionWithOptions(final String questionId, final String questionText,
                               final List<QuestionAnswer> answersList) {
        this.questionId = questionId;
        this.questionText = questionText;
        this.answersList = answersList;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public List<QuestionAnswer> getAnswersList() {
        return answersList;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QuestionWithOptions that = (QuestionWithOptions) o;
        return questionId.equals(that.questionId) && questionText.equals(that.questionText) && answersList.equals(that.answersList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId, questionText, answersList);
    }

    @Override
    public String toString() {
        return "QuestionWithOptions{" +
                "questionId='" + questionId + '\'' +
                ", questionText='" + questionText + '\'' +
                ", answersList=" + answersList +
                '}';
    }
}
