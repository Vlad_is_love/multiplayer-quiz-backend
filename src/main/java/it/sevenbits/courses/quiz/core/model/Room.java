package it.sevenbits.courses.quiz.core.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.HashMap;

/**
 * Room class
 */
public class Room {
    private final String roomId;
    private final String roomName;
    private final List<Map<String, String>> players;
    private final String ownerId;
    /**
     * Constructor
     * @param roomName name of the room
     * @param ownerId id of the owner of the room
     */
    public Room(final String roomName, final String ownerId) {
        this.players = new ArrayList<>();
        this.roomName = roomName;
        UUID uuid = UUID.randomUUID();
        roomId = uuid.toString();
        this.ownerId = ownerId;
    }
    /**
     * Constructor for creating instance from db
     * @param roomName name of the room
     * @param roomId id of the room
     * @param players list of players in the room
     * @param ownerId id of the owner of the room
     */
    public Room(final String roomId, final String roomName,
                final List<Map<String, String>> players, final String ownerId) {
        this.players = players;
        this.roomId = roomId;
        this.roomName = roomName;
        this.ownerId = ownerId;
    }
    /**
     * method that allows to add player to the room
     * @param playerId id of the player
     */
    public void addPlayer(final String playerId) {
        Map<String, String> player = new HashMap<>();
        player.put("playerId", playerId);
        players.add(player);
    }

    public String getRoomId() {
        return roomId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getRoomName() {
        return roomName;
    }

    public List<Map<String, String>> getPlayers() {
        return players;
    }
}
