package it.sevenbits.courses.quiz.core.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
/**
 * Config for database
 */
@Configuration
public class DatabaseConfig {
    /**
     * method for getting DataSource instance for our db
     * @return DataSource instance for our db
     */
    @Bean
    @FlywayDataSource
    @Qualifier("tasksDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.tasks")
    public DataSource tasksDataSource() {
        return DataSourceBuilder.create().build();
    }

    /**
     * method for getting JdbcOperations instance for our db
     * @param tasksDataSource instance of DataSource for our db
     * @return JdbcOperations instance for our db
     */
    @Bean
    @Qualifier("tasksJdbcOperations")
    public JdbcOperations tasksJdbcOperations(
            @Qualifier("tasksDataSource")
            final DataSource tasksDataSource
    ) {
        return new JdbcTemplate(tasksDataSource);
    }
}
