package it.sevenbits.courses.quiz.core.config;

import it.sevenbits.courses.quiz.web.controller.UserCredentialsResolver;
import it.sevenbits.courses.quiz.web.security.JwtAuthInterceptor;
import it.sevenbits.courses.quiz.web.security.JwtTokenService;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
/**
 * Class for web configuration
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final JwtTokenService jwtTokenService;
    /**
     * Constructor
     * @param jwtTokenService service for work with jwt tken
     */
    public WebConfiguration(
            final JwtTokenService jwtTokenService
    ) {
        this.jwtTokenService = jwtTokenService;
    }


    @Override
    public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new UserCredentialsResolver());
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor(
                new JwtAuthInterceptor(jwtTokenService)
        );
    }

}
