package it.sevenbits.courses.quiz.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Game status
 */
public class GameStatus {
    private final String status;
    private final String questionId;
    private final Integer questionNumber;
    private final Integer questionsCount;
    /**
     * Constructor
     * @param status status of the game
     * @param questionId id of the current question
     * @param questionNumber number of questions in game
     * @param questionsCount current question number
     */
    public GameStatus(@JsonProperty("status") final String status,
                      @JsonProperty("questionId") final String questionId,
                      @JsonProperty("questionNumber") final Integer questionNumber,
                      @JsonProperty("questionsCount") final Integer questionsCount) {
        this.status = status;
        this.questionId = questionId;
        this.questionNumber = questionNumber;
        this.questionsCount = questionsCount;
    }

    public String getStatus() {
        return status;
    }

    public String getQuestionId() {
        return questionId;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public Integer getQuestionsCount() {
        return questionsCount;
    }
}
