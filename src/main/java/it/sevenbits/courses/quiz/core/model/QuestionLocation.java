package it.sevenbits.courses.quiz.core.model;

/**
 * question location model
 */
public class QuestionLocation {
    private final String questionId;

    /**
     * Constructor
     * @param questionId id of the question
     */
    public QuestionLocation(final String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionId() {
        return questionId;
    }
}
