package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;

import java.util.List;

/**
 * IFace for question repository
 */
public interface IQuestionRepository {
    /**
     * method that allows to get full question
     * @param questionId id of the question of which to get
     * @return full answer
     */
    QuestionWithOptionsAndCorrectAnswer getFullQuestion(String questionId);

    /**
     * method that allows to get all questions ids
     * @return all questions ids
     */
    List<String> getQuestionsIds();
}
