package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Simple implementation for IGameRepository
 */
@Repository
public class SimpleGameRepository implements IGameRepository {
    private Map<String, List<String>> questionIds;
    private Map<String, QuestionWithOptionsAndCorrectAnswer> currentQuestion;

    private Map<String, String> currentQuestionId;
    private Map<String, Integer> score;
    private Map<String, Boolean> gameStarted;
    private Map<String, Integer> numOfAnsweredQuestions;
    private Map<String, Integer> questionsNumber;
    /**
     * Constructor
     */
    public SimpleGameRepository() {
        questionIds = new HashMap<>();
        currentQuestion = new HashMap<>();
        currentQuestionId = new HashMap<>();
        score = new HashMap<>();
        gameStarted = new HashMap<>();
        numOfAnsweredQuestions = new HashMap<>();
        questionsNumber = new HashMap<>();
    }
    @Override
    public List<String> getQuestionIds(final String roomId) {
        return questionIds.getOrDefault(roomId, null);
    }
    @Override
    public void setQuestionIds(final String roomId, final List<String> ids) {
        questionIds.put(roomId, ids);
        questionsNumber.put(roomId, questionIds.size());
    }
    @Override
    public void setGameStarted(final String roomId) {
        gameStarted.put(roomId, true);
    }
    @Override
    public void setCurrentQuestionId(final String roomId, final String questionId) {
        currentQuestionId.put(roomId, questionId);
    }
    @Override
    public void setCurrentQuestion(final String roomId, final QuestionWithOptionsAndCorrectAnswer question) {
        currentQuestion.put(roomId, question);
    }
    @Override
    public QuestionWithOptionsAndCorrectAnswer getCurrentQuestion(final String roomId) {
        return currentQuestion.getOrDefault(roomId, null);
    }
    @Override
    public Integer getNumOfAnsweredQuestions(final String roomId) {
        return numOfAnsweredQuestions.getOrDefault(roomId, null);
    }
    @Override
    public void setNumOfAnsweredQuestions(final String roomId, final Integer num) {
        numOfAnsweredQuestions.put(roomId, num);
    }
    @Override
    public Integer getScore(final String roomId) {
        return score.getOrDefault(roomId, null);
    }
    @Override
    public void setScore(final String roomId, final Integer scoreValue) {
        score.put(roomId, scoreValue);
    }
    @Override
    public Boolean isGameStarted(final String roomId) {
        return gameStarted.getOrDefault(roomId, null);
    }
    @Override
    public String getCurrentQuestionId(final String roomId) {
        return currentQuestionId.getOrDefault(roomId, null);
    }
    @Override
    public Integer getQuestionsNumber(final String roomId) {
        return questionsNumber.getOrDefault(roomId, null);
    }

    @Override
    public void createGameForRoom(final String roomId) {
    }
}
