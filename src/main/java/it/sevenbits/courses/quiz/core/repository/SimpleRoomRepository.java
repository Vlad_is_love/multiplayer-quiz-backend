package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.Room;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
 * Simple IRoomRepository implementation
 */
@Repository
public class SimpleRoomRepository implements IRoomRepository {
    private final List<Room> rooms;
    /**
     * Constructor
     */
    public SimpleRoomRepository() {
        this.rooms = new ArrayList<>();
    }

    @Override
    public Room getRoomById(final String roomId) {
        for (Room room : rooms) {
            if (roomId.equals(room.getRoomId())) {
                return room;
            }
        }
        return null;
    }

    @Override
    public List<Room> getRooms() {
        return rooms;
    }

    @Override
    public Room createRoom(final Room room) {
        rooms.add(room);
        return room;
    }

    @Override
    public Room updateRoom(final Room room) {
        return room;
    }
}
