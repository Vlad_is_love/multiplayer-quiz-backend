package it.sevenbits.courses.quiz.core.model;

import java.util.Objects;
import java.util.UUID;

/**
 * question answer model
 */
public class QuestionAnswer {
    private final String answerId;
    private final String answerText;

    /**
     * Constructor
     * @param answerText text of the answer
     */
    public QuestionAnswer(final String answerText) {
        this(UUID.randomUUID().toString(), answerText);
    }
    /**
     * Constructor for existing id
     * @param answerId id of the answer
     * @param answerText text of the answer
     */
    public QuestionAnswer(final String answerId, final String answerText) {
        this.answerId = answerId;
        this.answerText = answerText;
    }

    public String getAnswerId() {
        return answerId;
    }

    public String getAnswerText() {
        return answerText;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QuestionAnswer that = (QuestionAnswer) o;
        return answerId.equals(that.answerId) && answerText.equals(that.answerText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answerId, answerText);
    }
}
