package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.QuestionAnswer;
import it.sevenbits.courses.quiz.core.model.QuestionAnswerWithStatus;
import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;
import org.springframework.jdbc.core.JdbcOperations;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
/**
 * Database implementation for IGameRepository
 */
public class DatabaseGameRepository implements IGameRepository {
    private final JdbcOperations jdbcOperations;
    /**
     * Constructor
     * @param jdbcOperations JdbcOperations instance for our db
     */
    public DatabaseGameRepository(final JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public List<String> getQuestionIds(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT questionIds FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    Array arr = resultSet.getArray("questionIds");
                    if (arr != null) {
                        return new LinkedList<>(Arrays.asList((String[]) arr.getArray()));
                    } else {
                        return null;
                    }
                },
                roomId);
    }

    @Override
    public void setQuestionIds(final String roomId, final List<String> ids) {
        Integer questionsNumber = jdbcOperations.queryForObject(
                "SELECT questionsNumber FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    return resultSet.getInt("questionsNumber");
                },
                roomId);
        if (questionsNumber == 0) {
            jdbcOperations.update(
                    "UPDATE game SET questionsNumber = ? WHERE roomId = ?;",
                    ids.size(), roomId
            );
        }
        String[] sqlArrayOfIds = ids.toArray(new String[ids.size()]);
        jdbcOperations.update(
                "UPDATE game SET questionIds = ? WHERE roomId = ?;",
                sqlArrayOfIds, roomId
        );
    }

    @Override
    public void setGameStarted(final String roomId) {
        jdbcOperations.update(
                "UPDATE game SET gameStarted = ? WHERE roomId = ?;",
                true, roomId
        );
    }

    @Override
    public void setCurrentQuestionId(final String roomId, final String questionId) {
        jdbcOperations.update(
                "UPDATE game SET currentQuestionId = ? WHERE roomId = ?;",
                questionId, roomId
        );
    }

    @Override
    public void setCurrentQuestion(final String roomId, final QuestionWithOptionsAndCorrectAnswer question) {
    }

    @Override
    public QuestionWithOptionsAndCorrectAnswer getCurrentQuestion(final String roomId) {
        String questionId = getCurrentQuestionId(roomId);
        String questionText = jdbcOperations.queryForObject(
                "SELECT text FROM question WHERE questionId = ?;",
                (resultSet, i) -> {
                    return resultSet.getString("text");
                },
                questionId);
        List<QuestionAnswerWithStatus> questionAnswersWithStatus = jdbcOperations.query(
                "SELECT answerId, text, correct FROM answer WHERE questionId = ?;",
                (resultSet, i) -> {
                    String answerId = resultSet.getString("answerId");
                    String text = resultSet.getString("text");
                    boolean correct = resultSet.getBoolean("correct");
                    return new QuestionAnswerWithStatus(answerId, text, correct);
                },
                questionId);
        List<QuestionAnswer> answers = new ArrayList<>();
        QuestionAnswer correctAnswer = null;
        for (QuestionAnswerWithStatus question : questionAnswersWithStatus) {
            if (question.isCorrect()) {
                correctAnswer = new QuestionAnswer(question.getAnswerId(), question.getAnswerText());
            }
            answers.add(new QuestionAnswer(question.getAnswerId(), question.getAnswerText()));
        }
        QuestionWithOptionsAndCorrectAnswer question = new QuestionWithOptionsAndCorrectAnswer(
                questionId, questionText, answers, correctAnswer
        );
        return question;
    }

    @Override
    public Integer getNumOfAnsweredQuestions(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT numOfAnsweredQuestions FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    return resultSet.getInt("numOfAnsweredQuestions");
                },
                roomId);
    }

    @Override
    public void setNumOfAnsweredQuestions(final String roomId, final Integer num) {
        jdbcOperations.update(
                "UPDATE game SET numOfAnsweredQuestions = ? WHERE roomId = ?;",
                num, roomId
        );
    }

    @Override
    public Integer getScore(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT score FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    return resultSet.getInt("score");
                },
                roomId);
    }

    @Override
    public void setScore(final String roomId, final Integer scoreValue) {
        jdbcOperations.update(
                "UPDATE game SET score = ? WHERE roomId = ?;",
                scoreValue, roomId
        );
    }

    @Override
    public Boolean isGameStarted(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT gameStarted FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    return resultSet.getBoolean("gameStarted");
                },
                roomId);
    }

    @Override
    public String getCurrentQuestionId(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT currentQuestionId FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    return resultSet.getString("currentQuestionId");
                },
                roomId);
    }

    @Override
    public Integer getQuestionsNumber(final String roomId) {
        return jdbcOperations.queryForObject(
                "SELECT questionsNumber FROM game WHERE roomId = ?;",
                (resultSet, i) -> {
                    return resultSet.getInt("questionsNumber");
                },
                roomId);
    }

    @Override
    public void createGameForRoom(final String roomId) {
        int rows = jdbcOperations.update(
                "INSERT INTO game (roomId) VALUES (?);",
                roomId
        );
    }
}
