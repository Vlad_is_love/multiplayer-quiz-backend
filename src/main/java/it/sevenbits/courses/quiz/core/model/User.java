package it.sevenbits.courses.quiz.core.model;

import java.util.List;
/**
 * Class for user model
 */
public class User {
    private final String userId;
    private final String email;
    private final String password;
    private final List<String> roles;
    /**
     * Constructor
     * @param userId id of the user
     * @param email user email
     * @param password user password
     * @param roles user roles
     */
    public User(final String userId, final String email, final String password, final List<String> roles) {
        this.userId = userId;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getRoles() {
        return roles;
    }
}
