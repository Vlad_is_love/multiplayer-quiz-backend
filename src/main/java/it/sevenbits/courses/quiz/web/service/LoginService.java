package it.sevenbits.courses.quiz.web.service;

import it.sevenbits.courses.quiz.core.exception.LoginFailedException;
import it.sevenbits.courses.quiz.core.model.User;
import it.sevenbits.courses.quiz.core.repository.IUserRepository;
import it.sevenbits.courses.quiz.core.security.PasswordEncoder;
import org.springframework.stereotype.Service;
/**
 * Service for users login
 */
@Service
public class LoginService {

    private final IUserRepository users;
    private final PasswordEncoder passwordEncoder;
    /**
     * Constructor
     * @param users repository containing all users
     * @param passwordEncoder instance of class that encodes and matches password
     */
    public LoginService(final IUserRepository users, final PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }
    /**
     * method that allows user to log in
     * @param email user email
     * @param password user password
     * @return user
     */
    public User login(final String email, final String password) {
        User user = users.getUserByEmail(email);

        if (user == null) {
            throw new LoginFailedException(
                    "User '" + email + "' not found"
            );
        }

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new LoginFailedException("Wrong password");
        }

        return user;
    }
}