package it.sevenbits.courses.quiz.web.controller;

import it.sevenbits.courses.quiz.core.model.User;
import it.sevenbits.courses.quiz.web.model.SignInRequest;
import it.sevenbits.courses.quiz.web.model.SignInResponse;
import it.sevenbits.courses.quiz.web.security.JwtTokenService;
import it.sevenbits.courses.quiz.web.service.LoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Controller class for user log in
 */
@Controller
@RequestMapping("/signin")
public class LoginController {
    private final LoginService loginService;
    private final JwtTokenService tokenService;
    /**
     * Constructor
     * @param loginService service for user log in
     * @param tokenService service for working with jwt token
     */
    public LoginController(final LoginService loginService, final JwtTokenService tokenService) {
        this.loginService = loginService;
        this.tokenService = tokenService;
    }
    /**
     * method that allows to sign in
     * @param request request containing data for sign in
     * @return sign in response containing access token
     */
    @PostMapping()
    public ResponseEntity<SignInResponse> signIn(@RequestBody final SignInRequest request) {
        User user = loginService.login(request.getEmail(), request.getPassword());
        String token = tokenService.createToken(user);
        return ResponseEntity.ok().body(new SignInResponse(token));
    }
}
