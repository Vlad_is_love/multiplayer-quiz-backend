package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Response class for sign in
 */
public class SignInResponse {
    private final String accessToken;
    /**
     * Constructor
     * @param accessToken access token for user
     */
    public SignInResponse(@JsonProperty("accessToken") final String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
