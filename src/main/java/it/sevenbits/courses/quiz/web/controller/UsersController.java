package it.sevenbits.courses.quiz.web.controller;

import it.sevenbits.courses.quiz.core.model.User;
import it.sevenbits.courses.quiz.core.repository.IUserRepository;
import it.sevenbits.courses.quiz.web.model.GetUserResponse;
import it.sevenbits.courses.quiz.web.model.GetUsersResponse;
import it.sevenbits.courses.quiz.web.security.AuthRoleRequired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
/**
 * Controller for users requests
 */
@Controller
@RequestMapping("/users")
public class UsersController {
    private final IUserRepository usersRepository;
    /**
     * Constructor
     * @param usersRepository repository containing all users
     */
    public UsersController(final IUserRepository usersRepository) {
        this.usersRepository = usersRepository;
    }
    /**
     * method that allows to get all users
     * @return users list
     */
    @GetMapping
    @ResponseBody
    @AuthRoleRequired("ADMIN")
    public ResponseEntity<List<GetUsersResponse>> getAllUsers() {
        List<User> users = usersRepository.getAllUsers();
        List<GetUsersResponse> responses = new ArrayList<>();
        for (User user : users) {
            responses.add(new GetUsersResponse(user.getUserId(), user.getEmail(), user.getRoles()));
        }
        return ResponseEntity.ok(responses);
    }
    /**
     * method that allows to get user info
     * @param id user id
     * @return users
     */
    @GetMapping(value = "/{id}")
    @ResponseBody
    @AuthRoleRequired("ADMIN")
    public ResponseEntity<GetUserResponse> getUserInfo(@PathVariable("id") final String id) {
        User user = usersRepository.getUserById(id);
        GetUserResponse response = new GetUserResponse(user.getUserId(), user.getEmail(), user.getRoles());
        return ResponseEntity.ok().body(response);
    }
}

