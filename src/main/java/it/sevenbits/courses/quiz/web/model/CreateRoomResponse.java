package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;
/**
 * Class for create room response
 */
public class CreateRoomResponse {
    private final String roomId;
    private final String roomName;
    private final String ownerId;
    private final List<Map<String, String>> players;
    /**
     * Constructor
     * @param roomId id of the room
     * @param roomName name of the room
     * @param players players in the room
     * @param ownerId room owner id
     */
    public CreateRoomResponse(@JsonProperty("roomId") final String roomId,
                              @JsonProperty("roomName") final String roomName,
                              @JsonProperty("players") final List<Map<String, String>> players,
                              @JsonProperty("ownerId") final String ownerId) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.players = players;
        this.ownerId = ownerId;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getRoomName() {
        return roomName;
    }

    public List<Map<String, String>> getPlayers() {
        return players;
    }
}
