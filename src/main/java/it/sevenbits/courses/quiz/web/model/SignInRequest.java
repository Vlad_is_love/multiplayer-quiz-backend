package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Request class for sign in
 */
public class SignInRequest {
    private final String email;
    private final String password;
    /**
     * Constructor
     * @param email user email
     * @param password user password
     */
    public SignInRequest(@JsonProperty("email") final String email,
                         @JsonProperty("password") final String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
