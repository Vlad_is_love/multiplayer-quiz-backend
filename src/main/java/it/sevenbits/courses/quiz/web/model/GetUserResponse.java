package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
/**
 * Response class for get user method
 */
public class GetUserResponse {
    private final String userId;
    private final String email;
    private final List<String> roles;
    /**
     * Constructor
     * @param userId id of the user
     * @param email email of the user
     * @param roles roles of the user
     */
    public GetUserResponse(@JsonProperty("userId") final String userId,
                           @JsonProperty("email") final String email,
                           @JsonProperty("roles") final List<String> roles) {
        this.userId = userId;
        this.email = email;
        this.roles = roles;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getRoles() {
        return roles;
    }
}
