package it.sevenbits.courses.quiz.web.service;

import it.sevenbits.courses.quiz.core.exception.GameRuleErrorCode;
import it.sevenbits.courses.quiz.core.exception.GameRuleException;
import it.sevenbits.courses.quiz.core.model.Room;
import it.sevenbits.courses.quiz.core.repository.IGameRepository;
import it.sevenbits.courses.quiz.core.repository.IRoomRepository;
import it.sevenbits.courses.quiz.web.model.CreateRoomRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Room service
 */
@Service
public class RoomService {
    private final IRoomRepository roomRepository;
    private final IGameRepository gameRepository;


    /**
     * Constructor
     * @param roomRepository repository which contains rooms
     * @param gameRepository repository for saving game state in room
     */
    public RoomService(final IRoomRepository roomRepository, final IGameRepository gameRepository) {
        this.roomRepository = roomRepository;
        this.gameRepository = gameRepository;
    }

    /**
     * method for creating room
     * @param request request containing room name and player id
     * @param playerId id of the player
     * @throws IOException standard java exception
     * @return id of the new room
     */
    public String createRoom(final CreateRoomRequest request, final String playerId) throws IOException {
        Room newRoom = new Room(request.getRoomName(), playerId);
        newRoom.addPlayer(playerId);
        roomRepository.createRoom(newRoom);
        gameRepository.createGameForRoom(newRoom.getRoomId());
        return newRoom.getRoomId();
    }

    public List<Room> getRooms() {
        return roomRepository.getRooms();
    }

    /**
     * method that allows to get room by its id
     * @param roomId id of the room which to get
     * @return room
     * @throws GameRuleException exception which occurs in wrong game logic
     */
    public Room getRoomById(final String roomId) throws GameRuleException {
        Room room = roomRepository.getRoomById(roomId);
        if (room != null) {
            return room;
        } else {
            throw new GameRuleException(GameRuleErrorCode.ROOM_NOT_FOUND);
        }
    }
    /**
     * method that puts player in a room
     * @param roomId id of the room in which to join
     * @param playerId id of the player
     * @throws GameRuleException exception which occurs in wrong game logic
     */
    public void joinRoom(final String roomId, final String playerId) throws GameRuleException {
        Room room = getRoomById(roomId);
        if (room != null) {
            room.addPlayer(playerId);
            roomRepository.updateRoom(room);
        } else {
            throw new GameRuleException(GameRuleErrorCode.ROOM_NOT_FOUND);
        }
    }
}
