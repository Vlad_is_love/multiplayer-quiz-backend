package it.sevenbits.courses.quiz.web.service;

import it.sevenbits.courses.quiz.core.model.User;
import it.sevenbits.courses.quiz.core.repository.IUserRepository;

import java.util.List;
/**
 * Service for users
 */
public class UserService {
    private final IUserRepository usersRepository;
    /**
     * Constructor
     * @param usersRepository repository containing all users
     */
    public UserService(final IUserRepository usersRepository) {
        this.usersRepository = usersRepository;
    }
    /**
     * method that allows to get all users
     * @return users list
     */
    public List<User> getAllUsers() {
        return usersRepository.getAllUsers();
    }
    /**
     * method that allows to get single user by id
     * @param id of the user
     * @return user
     */
    public User getUser(final String id) {
        return usersRepository.getUserById(id);
    }

}
