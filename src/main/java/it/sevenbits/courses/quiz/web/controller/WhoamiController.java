package it.sevenbits.courses.quiz.web.controller;

import it.sevenbits.courses.quiz.web.security.AuthRoleRequired;
import it.sevenbits.courses.quiz.web.security.UserCredentials;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller to display the current user.
 */
@Controller
@RequestMapping("/whoami")
public class WhoamiController {
    /**
     * method that allows to get credentials of current user
     * @param userCredentials user credentials
     * @return user credentials
     */
    @GetMapping
    @ResponseBody
    @AuthRoleRequired("USER")
    public ResponseEntity<UserCredentials> get(
            final UserCredentials userCredentials
    ) {
        return ResponseEntity.ok(userCredentials);
    }

}
