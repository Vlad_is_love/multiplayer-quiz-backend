package it.sevenbits.courses.quiz.web.controller;

import it.sevenbits.courses.quiz.web.model.SignUpRequest;
import it.sevenbits.courses.quiz.web.service.RegistrationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Controller class for user registration
 */
@Controller
@RequestMapping("/signup")
public class RegistrationController {
    private final RegistrationService registrationService;
    /**
     * Constructor
     * @param registrationService service for user registration
     */
    public RegistrationController(final RegistrationService registrationService) {
        this.registrationService = registrationService;
    }
    /**
     * method that allows to sign up new user
     * @param request request containing user data
     * @return http status ok
     */
    @PostMapping
    public ResponseEntity signUp(@RequestBody final SignUpRequest request) {
        registrationService.createNewUser(request.getEmail(), request.getPassword());
        return new ResponseEntity(HttpStatus.OK);
    }
}
