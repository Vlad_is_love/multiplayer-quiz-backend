package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class for getting all rooms response
 */
public class GetRoomsResponse {
    private final String roomId;
    private final String roomName;

    private final String ownerId;
    /**
     * Constructor
     * @param roomId id of the room
     * @param roomName name of the room
     * @param ownerId room owner id
     */
    public GetRoomsResponse(@JsonProperty("roomId") final String roomId,
                            @JsonProperty("roomName") final String roomName,
                            @JsonProperty("ownerId") final String ownerId) {
        this.roomId = roomId;
        this.ownerId = ownerId;
        this.roomName = roomName;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getRoomName() {
        return roomName;
    }
}
