package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class for answer question request
 */
public class AnswerQuestionRequest {
    private final String answerId;
    /**
     * Constructor
     * @param answerId id of the answered question
     */
    public AnswerQuestionRequest(@JsonProperty("answerId") final String answerId) {
        this.answerId = answerId;
    }

    public String getAnswerId() {
        return answerId;
    }
}
