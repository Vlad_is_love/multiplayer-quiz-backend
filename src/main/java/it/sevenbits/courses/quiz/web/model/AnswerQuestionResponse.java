package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class for answer question response
 */
public class AnswerQuestionResponse {
    private final String correctAnswerId;
    private final String questionId;
    private final String nextQuestionId;
    private final Integer totalScore;
    private final Integer questionScore;

    /**
     * Class for answer question response
     * @param correctAnswerId id of the correct answer
     * @param questionId id of the current question
     * @param nextQuestionId id of the next question
     * @param totalScore score of the game
     * @param questionScore score of the current question
     */
    public AnswerQuestionResponse(@JsonProperty("correctAnswerId") final String correctAnswerId,
                                  @JsonProperty("questionId") final String questionId,
                                  @JsonProperty("nextQuestionId") final String nextQuestionId,
                                  @JsonProperty("totalScore") final Integer totalScore,
                                  @JsonProperty("questionScore") final Integer questionScore) {
        this.questionId = questionId;
        this.correctAnswerId = correctAnswerId;
        this.nextQuestionId = nextQuestionId;
        this.totalScore = totalScore;
        this.questionScore = questionScore;
    }

    public String getCorrectAnswerId() {
        return correctAnswerId;
    }

    public String getNextQuestionId() {
        return nextQuestionId;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public Integer getQuestionScore() {
        return questionScore;
    }

    public String getQuestionId() {
        return questionId;
    }
}
