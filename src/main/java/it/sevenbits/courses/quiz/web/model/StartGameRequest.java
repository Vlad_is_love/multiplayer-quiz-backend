package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class for starting game request
 */
public class StartGameRequest {
    private final String playerId;

    /**
     * Constructor
     * @param playerId id of the player
     */
    public StartGameRequest(@JsonProperty("playerId") final String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerId() {
        return playerId;
    }
}
