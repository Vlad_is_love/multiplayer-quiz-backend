package it.sevenbits.courses.quiz.web.service;

import it.sevenbits.courses.quiz.core.exception.GameProhibitedException;
import it.sevenbits.courses.quiz.core.exception.GameRuleErrorCode;
import it.sevenbits.courses.quiz.core.exception.GameRuleException;
import it.sevenbits.courses.quiz.core.model.GameStatus;
import it.sevenbits.courses.quiz.core.model.Room;
import it.sevenbits.courses.quiz.core.model.QuestionAnswer;
import it.sevenbits.courses.quiz.core.model.QuestionWithOptions;
import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;
import it.sevenbits.courses.quiz.core.repository.IGameRepository;
import it.sevenbits.courses.quiz.core.repository.IQuestionRepository;
import it.sevenbits.courses.quiz.core.repository.IRoomRepository;
import it.sevenbits.courses.quiz.web.model.AnswerQuestionRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Game service
 */
@Service
public class GameService {
    private final IQuestionRepository questionRepository;

    private final IGameRepository gameRepository;

    private final IRoomRepository roomRepository;


    /**
     * Constructor
     * @param questionRepository repository which allows getting questions
     * @param gameRepository repository for saving game state in room
     * @param roomRepository repository which contains rooms
     */
    public GameService(final IQuestionRepository questionRepository,
                       final IGameRepository gameRepository,
                       final IRoomRepository roomRepository) {
        this.gameRepository = gameRepository;
        this.questionRepository = questionRepository;
        this.roomRepository = roomRepository;
    }

    private String defineNextQuestion(final String roomId, final List<String> questionIds) throws GameRuleException {
        if (questionIds.size() != 0) {
            Random rand = new Random();
            int nextInt = rand.nextInt(questionIds.size());
            String id = questionIds.get(nextInt);
            questionIds.remove(id);
            gameRepository.setCurrentQuestionId(roomId, id);
            gameRepository.setQuestionIds(roomId, questionIds);
            return id;
        } else {
            throw new GameRuleException(GameRuleErrorCode.EMPTY_POOL_OF_QUESTIONS);
        }
    }

    /**
     * method that allows to get next question id
     * @param roomId id of the room
     * @param playerId id of the player
     * @return next question id
     * @throws GameRuleException exception which occurs in wrong game logic
     * @throws GameProhibitedException exception which occurs if something is prohibited for player
     */
    public String getNextQuestionId(final String roomId, final String playerId) throws GameRuleException, GameProhibitedException {
        if (isPlayerInRoom(roomId, playerId)) {
            gameRepository.setGameStarted(roomId);
            List<String> questionIds = gameRepository.getQuestionIds(roomId);
            if (questionIds == null) {
                questionIds = questionRepository.getQuestionsIds();
                gameRepository.setQuestionIds(roomId, questionIds);
            }
            return defineNextQuestion(roomId, questionIds);
        } else {
            throw new GameProhibitedException(GameRuleErrorCode.PLAYER_NOT_IN_ROOM);
        }
    }

    /**
     * method that allows to get next question
     * @param roomId id of the room
     * @param questionId id of the question of which to get
     * @throws GameRuleException exception which occurs in wrong game logic
     * @return question with options
     */
    public QuestionWithOptions getNextQuestion(final String roomId, final String questionId) throws GameRuleException {
        QuestionWithOptionsAndCorrectAnswer fullQuestion = questionRepository.getFullQuestion(questionId);
        if (fullQuestion != null) {
            gameRepository.setCurrentQuestion(roomId, fullQuestion);
            return new QuestionWithOptions(fullQuestion.getQuestionId(),
                    fullQuestion.getQuestionText(), fullQuestion.getAnswersList());
        } else {
            throw new GameRuleException(GameRuleErrorCode.EMPTY_POOL_OF_QUESTIONS);
        }
    }

    /**
     * method that allows to check if answer is correct
     * @param questionId id of the question
     * @param chosenAnswer answer which to check
     * @param roomId id of the room
     * @param playerId id of the player
     * @return correct answer
     * @throws GameRuleException exception which occurs in wrong game logic
     * @throws GameProhibitedException exception which occurs if something is prohibited for player
     */
    public QuestionAnswer checkAnswer(final String roomId, final String questionId,
                                      final AnswerQuestionRequest chosenAnswer,
                                      final String playerId) throws GameRuleException, GameProhibitedException {
        if (isPlayerInRoom(roomId, playerId)) {
            boolean appropriateAnswer = false;
            QuestionWithOptionsAndCorrectAnswer currentQuestion = gameRepository.getCurrentQuestion(roomId);
            if (currentQuestion == null) {
                throw new GameRuleException(GameRuleErrorCode.INAPPROPRIATE_ANSWER_OR_WRONG_QUESTION);
            }
            for (QuestionAnswer answer : currentQuestion.getAnswersList()) {
                appropriateAnswer = chosenAnswer.getAnswerId().equals(answer.getAnswerId());
                if (appropriateAnswer) {
                    break;
                }
            }
            if (!questionId.equals(currentQuestion.getQuestionId()) || !appropriateAnswer) {
                throw new GameRuleException(GameRuleErrorCode.INAPPROPRIATE_ANSWER_OR_WRONG_QUESTION);
            } else {
                updateScore(roomId, chosenAnswer);
                return currentQuestion.getCorrectAnswer();
            }
        } else {
            throw new GameProhibitedException(GameRuleErrorCode.PLAYER_NOT_IN_ROOM);
        }
    }

    private void updateScore(final String roomId, final AnswerQuestionRequest chosenAnswer) {
        Integer numOfAnsweredQuestions = gameRepository.getNumOfAnsweredQuestions(roomId);
        if (numOfAnsweredQuestions == null) {
            numOfAnsweredQuestions = 0;
            gameRepository.setNumOfAnsweredQuestions(roomId, numOfAnsweredQuestions);
        }
        numOfAnsweredQuestions++;
        gameRepository.setNumOfAnsweredQuestions(roomId, numOfAnsweredQuestions);
        QuestionWithOptionsAndCorrectAnswer currentQuestion = gameRepository.getCurrentQuestion(roomId);
        if (chosenAnswer.getAnswerId().equals(currentQuestion.getCorrectAnswer().getAnswerId())) {
            Integer score = gameRepository.getScore(roomId);
            if (score == null) {
                score = 0;
                gameRepository.setScore(roomId, score);
            }
            score++;
            gameRepository.setScore(roomId, score);
        }
    }
    /**
     * method that allows to get game score
     * @param roomId id of the room
     * @return score - score of the game
     */
    public Integer getGameScore(final String roomId) {
        Integer score = gameRepository.getScore(roomId);
        if (score == null) {
            score = 0;
            gameRepository.setScore(roomId, score);
        }
        return score;
    }

    /**
     * method that allows to get game status
     * @param roomId id of the room
     * @return game status
     */
    public GameStatus getGameStatus(final String roomId) {
        String gameStatus;
        GameStatus result;
        Boolean gameStarted = gameRepository.isGameStarted(roomId);
        if (gameStarted == null || !gameStarted) {
            gameStatus = "Waiting to start";
        } else {
            gameStatus = "Started";
        }
        String currentQuestionId = gameRepository.getCurrentQuestionId(roomId);
        Integer questionsNumber = gameRepository.getQuestionsNumber(roomId);
        Integer numOfAnsweredQuestions = gameRepository.getNumOfAnsweredQuestions(roomId);
        result = new GameStatus(gameStatus, currentQuestionId, questionsNumber, numOfAnsweredQuestions);
        return result;
    }
    /**
     * method that allows to get current question id
     * @param roomId id of the room
     * @return id of the question
     */
    public String getCurrentQuestionId(final String roomId) {
        return gameRepository.getCurrentQuestionId(roomId);
    }

    private boolean isPlayerInRoom(final String roomId, final String targetPlayerId) {
        boolean playerIsInRoom = false;
        Room room = roomRepository.getRoomById(roomId);
        for (Map<String, String> player : room.getPlayers()) {
            if (player.get("playerId").equals(targetPlayerId)) {
                playerIsInRoom = true;
                break;
            }
        }
        return playerIsInRoom;
    }
}
