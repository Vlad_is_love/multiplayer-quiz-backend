package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class for joining room request
 */
public class JoinRoomRequest {
    private final String playerId;

    /**
     * Constructor
     * @param playerId id of the player
     */
    public JoinRoomRequest(@JsonProperty("playerId") final String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerId() {
        return playerId;
    }
}
