package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class for create room request
 */
public class CreateRoomRequest {
    private final String roomName;
    /**
     * Constructor
     * @param roomName name of the room
     */
    public CreateRoomRequest(@JsonProperty("roomName") final String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }
}
