package it.sevenbits.courses.quiz.web.controller;
import it.sevenbits.courses.quiz.core.exception.GameProhibitedException;
import it.sevenbits.courses.quiz.core.exception.GameRuleException;
import it.sevenbits.courses.quiz.core.model.GameStatus;
import it.sevenbits.courses.quiz.core.model.QuestionAnswer;
import it.sevenbits.courses.quiz.core.model.QuestionLocation;
import it.sevenbits.courses.quiz.core.model.QuestionWithOptions;
import it.sevenbits.courses.quiz.web.model.AnswerQuestionResponse;
import it.sevenbits.courses.quiz.web.model.GetRoomResponse;
import it.sevenbits.courses.quiz.web.model.GetRoomsResponse;
import it.sevenbits.courses.quiz.web.security.AuthRoleRequired;
import it.sevenbits.courses.quiz.web.security.UserCredentials;
import it.sevenbits.courses.quiz.web.service.GameService;

import it.sevenbits.courses.quiz.core.model.Room;

import it.sevenbits.courses.quiz.web.model.CreateRoomRequest;
import it.sevenbits.courses.quiz.web.model.CreateRoomResponse;
import it.sevenbits.courses.quiz.web.model.AnswerQuestionRequest;
import it.sevenbits.courses.quiz.web.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Game controller
 */
@Controller
@RequestMapping("/")
public class GameController {
    private final RoomService roomService;
    private final GameService gameService;

    /**
     * Constructor
     * @param roomService - service for room logic
     * @param gameService - service for game logic
     */
    @Autowired
    public GameController(final RoomService roomService, final GameService gameService) {
        this.roomService = roomService;
        this.gameService = gameService;
    }
    /**
     * method that allows to get all rooms
     * @param credentials user credentials
     * @return rooms
     */
    @GetMapping("/rooms")
    @ResponseBody
    @AuthRoleRequired("USER")
    public ResponseEntity<List<GetRoomsResponse>> getRooms(final UserCredentials credentials) {
        List<Room> rooms = roomService.getRooms();
        List<GetRoomsResponse> response = new ArrayList<>();
        for (Room room : rooms) {
            response.add(new GetRoomsResponse(room.getRoomId(), room.getRoomName(), room.getOwnerId()));
        }
        return ResponseEntity.ok().body(response);
    }
    /**
     * method that allows to create room
     * @param request request that contains player id and room name
     * @param credentials user credentials
     * @return room
     * @throws IOException standard java exception
     * @throws GameRuleException exception which occurs in wrong game logic
     */
    @PostMapping("/rooms")
    @AuthRoleRequired("USER")
    public ResponseEntity<CreateRoomResponse> createRoom(@RequestBody final CreateRoomRequest request,
                                                         final UserCredentials credentials)
            throws IOException, GameRuleException {
        String roomId = roomService.createRoom(request, credentials.getUserId());
        Room room = roomService.getRoomById(roomId);
        CreateRoomResponse response = new CreateRoomResponse(roomId, room.getRoomName(), room.getPlayers(), room.getOwnerId());
        return ResponseEntity.ok().body(response);
    }
    /**
     * method that allows to get the room
     * @param id room id
     * @param credentials user credentials
     * @return room
     */
    @GetMapping("/rooms/{id}")
    @AuthRoleRequired("USER")
    public ResponseEntity<GetRoomResponse> getRoom(@PathVariable final String id,
                                                   final UserCredentials credentials) {
        try {
            Room room = roomService.getRoomById(id);
            GetRoomResponse response = new GetRoomResponse(room.getRoomId(), room.getRoomName(), room.getPlayers(), room.getOwnerId());
            return ResponseEntity.ok().body(response);
        } catch (GameRuleException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    /**
     * method that allows to join the room
     * @param id room id
     * @param credentials user credentials
     * @return 200 code
     */
    @PostMapping("/rooms/{id}/join")
    @AuthRoleRequired("USER")
    public ResponseEntity joinRoom(@PathVariable final String id,
                                   final UserCredentials credentials) {
        try {
            roomService.joinRoom(id, credentials.getUserId());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (GameRuleException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    /**
     * method that allows to get game status
     * @param id room id
     * @param credentials user credentials
     * @return game status
     */
    @GetMapping("/rooms/{id}/game")
    @AuthRoleRequired("USER")
    public ResponseEntity<GameStatus> getGameStatus(@PathVariable final String id,
                                                    final UserCredentials credentials) {
        try {
            Room room = roomService.getRoomById(id);
            GameStatus gameStatus = gameService.getGameStatus(id);
            return ResponseEntity.ok().body(gameStatus);
        } catch (GameRuleException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    /**
     * method that starts game
     * @param roomId room id
     * @param credentials user credentials
     * @return next question id
     * @throws GameRuleException exception which occurs in wrong game logic
     */
    @PostMapping("/rooms/{roomId}/game/start")
    @AuthRoleRequired("USER")
    public ResponseEntity<QuestionLocation> startGame(
            @PathVariable final String roomId,
            final UserCredentials credentials) throws GameRuleException {
        try {
            Room room = roomService.getRoomById(roomId);
            if (!room.getOwnerId().equals(credentials.getUserId())) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            try {
                QuestionLocation questionLocation = new QuestionLocation(
                        gameService.getNextQuestionId(roomId, credentials.getUserId()));
                return ResponseEntity.ok().body(questionLocation);
            } catch (GameProhibitedException exception) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        } catch (GameRuleException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * method that allows to get answer
     * @param roomId room id
     * @param credentials user credentials
     * @param questionId - question which to get
     * @return question
     */
    @GetMapping("/rooms/{roomId}/game/question/{questionId}")
    @AuthRoleRequired("USER")
    public ResponseEntity<QuestionWithOptions> getNextQuestion(@PathVariable final String roomId,
                                                               @PathVariable final String questionId,
                                                               final UserCredentials credentials) {
        try {
            QuestionWithOptions questionWithOptions = gameService.getNextQuestion(roomId, questionId);
            return ResponseEntity.ok().body(questionWithOptions);
        } catch (GameRuleException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * method that allows to answer question
     * @param roomId room id
     * @param questionId - question which to answer
     * @param request contains JSON answer
     * @param credentials user credentials
     * @return correct answer and game score
     * @throws GameRuleException exception which occurs in wrong game logic
     */
    @PostMapping("/rooms/{roomId}/game/question/{questionId}/answer")
    @AuthRoleRequired("USER")
    public ResponseEntity<AnswerQuestionResponse> answerQuestion(
            @PathVariable final String roomId,
            @RequestBody final AnswerQuestionRequest request,
            @PathVariable final String questionId,
            final UserCredentials credentials) throws GameRuleException {
            try {
                Room room = roomService.getRoomById(roomId);
            } catch (GameRuleException exception) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            try {
                QuestionAnswer correctAnswer = gameService.checkAnswer(roomId, questionId, request, credentials.getUserId());
                Integer questionScore = 0;
                if (correctAnswer.getAnswerId().equals(request.getAnswerId())) {
                    questionScore++;
                }
                AnswerQuestionResponse response = new AnswerQuestionResponse(correctAnswer.getAnswerId(),
                        gameService.getCurrentQuestionId(roomId),
                        gameService.getNextQuestionId(roomId, credentials.getUserId()),
                        gameService.getGameScore(roomId),
                        questionScore);

                return ResponseEntity.ok().body(response);
            } catch (GameProhibitedException exception) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
    }
}
