package it.sevenbits.courses.quiz.web.service;

import it.sevenbits.courses.quiz.core.model.User;
import it.sevenbits.courses.quiz.core.repository.IUserRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
/**
 * Service for user registration
 */
@Service
public class RegistrationService {
    private final IUserRepository userRepository;
    /**
     * Constructor
     * @param userRepository repository containing all users
     */
    public RegistrationService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }
    /**
     * method that allows to create new user
     * @param email user email
     * @param password user password
     * @return user
     */
    public User createNewUser(final String email, final String password) {
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        return userRepository.addUser(email, hashedPassword);
    }
}
