package it.sevenbits.courses.quiz.web.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
/**
 * Class for user credentials parsed from token
 */
class UserCredentialsImpl implements UserCredentials {

    @JsonProperty("email")
    private final String email;

    @JsonProperty("roles")
    private final Set<String> roles;

    @JsonProperty("userId")
    private final String userId;
    /**
     * Constructor
     * @param email user email
     * @param roles user roles
     * @param userId user id
     */
    @JsonCreator
    UserCredentialsImpl(final String email, final Collection<String> roles, final String userId) {
        this.email = email;
        this.userId = userId;
        this.roles = Collections.unmodifiableSet(new LinkedHashSet<>(roles));
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public Set<String> getRoles() {
        return roles;
    }

}
