package it.sevenbits.courses.quiz.web.security;

import java.util.Set;
/**
 * Interface for user credentials
 */
public interface UserCredentials {
    /**
     * method for getting email
     * @return email of the user
     */
    String getEmail();
    /**
     * method for getting user roles
     * @return roles set of the user
     */
    Set<String> getRoles();
    /**
     * method for getting user id
     * @return id of the user
     */
    String getUserId();
}
