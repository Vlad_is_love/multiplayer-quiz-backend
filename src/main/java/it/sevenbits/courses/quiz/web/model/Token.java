package it.sevenbits.courses.quiz.web.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class for jwt token
 */
public class Token {

    private final String token;
    /**
     * Constructor
     * @param token jwt token
     */
    @JsonCreator
    public Token(@JsonProperty("token") final String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
