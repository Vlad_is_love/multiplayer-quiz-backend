package it.sevenbits.courses.quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class of the app
 */
@SpringBootApplication
public class Application {
    /**
     * method that starts the app
     * @param args - console arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
