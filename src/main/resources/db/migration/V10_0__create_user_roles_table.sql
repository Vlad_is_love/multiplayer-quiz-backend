create table user_roles (
  email  VARCHAR(256) REFERENCES users(email) ON DELETE CASCADE,
  role VARCHAR(256),
  PRIMARY KEY (email, role)
);