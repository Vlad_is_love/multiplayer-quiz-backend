create table if not exists answer (
answerId varchar primary key,
text varchar not null,
questionId varchar not null,
correct boolean not null,
FOREIGN KEY (questionId)
      REFERENCES question (questionId)
);