create table if not exists game (
roomId VARCHAR PRIMARY KEY,
questionIds VARCHAR[],
currentQuestionId VARCHAR,
score integer,
gameStarted boolean,
numOfAnsweredQuestions integer,
questionsNumber integer,
FOREIGN KEY (currentQuestionId)
      REFERENCES question (questionId));