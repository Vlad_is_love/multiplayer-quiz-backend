create table users (
  userId VARCHAR(256) NOT NULL,
  email VARCHAR(256) PRIMARY KEY,
  password VARCHAR(256),
  enabled boolean);