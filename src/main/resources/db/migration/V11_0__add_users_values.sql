insert into users values
('8f0fc24b-73c2-4222-a2d7-9c723552a204','user@example.com', '$2a$10$XS1MSWXAAbzfZaLbZu9TqullOfLJqb3cJFqUGCFlpwWUeK0zITfK.', true),
('78bc7657-58c0-4257-a6f0-7da03471e0ac','admin@example.com', '$2a$10$aVn5fpmI7HsfxYi1d6EG0OAspcN45mdsMKWbTWo/nS3KOPB6GncxC', true);