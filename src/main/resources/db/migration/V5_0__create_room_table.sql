create table if not exists room (
roomId VARCHAR PRIMARY KEY,
roomName VARCHAR NOT NULL,
playersIds VARCHAR[] NOT NULL,
ownerId VARCHAR NOT NULL);