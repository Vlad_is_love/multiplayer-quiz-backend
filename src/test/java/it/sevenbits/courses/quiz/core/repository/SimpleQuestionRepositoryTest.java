package it.sevenbits.courses.quiz.core.repository;

import it.sevenbits.courses.quiz.core.model.QuestionWithOptionsAndCorrectAnswer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class SimpleQuestionRepositoryTest {
    private SimpleQuestionRepository simpleQuestionRepository;

    @Before
    public void setUp() throws IOException {
        simpleQuestionRepository = new SimpleQuestionRepository();
    }

    @Test
    public void getQuestionsIdTestSuccess() {
        List<String> ids = simpleQuestionRepository.getQuestionsIds();
        assertTrue(ids.size() > 0);
    }

    @Test
    public void getFullQuestion() {
        List<String> ids = simpleQuestionRepository.getQuestionsIds();
        QuestionWithOptionsAndCorrectAnswer question = simpleQuestionRepository.getFullQuestion(ids.get(0));
        assertNotNull(question.getQuestionId());
        assertNotNull(question.getQuestionText());
        assertNotNull(question.getCorrectAnswer());
        assertNotNull(question.getAnswersList());
    }

}
