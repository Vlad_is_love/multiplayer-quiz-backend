package it.sevenbits.courses.quiz.web.service;


import it.sevenbits.courses.quiz.core.exception.GameRuleErrorCode;
import it.sevenbits.courses.quiz.core.exception.GameRuleException;
import it.sevenbits.courses.quiz.core.repository.SimpleGameRepository;
import it.sevenbits.courses.quiz.core.repository.SimpleRoomRepository;
import it.sevenbits.courses.quiz.web.model.CreateRoomRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class RoomServiceTest {
    private RoomService roomService;
    private CreateRoomRequest createRoomRequest;

    private String playerId;

    @Before
    public void SetUp(){
        roomService = new RoomService(new SimpleRoomRepository(), new SimpleGameRepository());
        createRoomRequest = new CreateRoomRequest("name");
        playerId = "44";
    }

    @Test
    public void getRoomByIdSuccess() throws IOException, GameRuleException {
        String roomId = roomService.createRoom(createRoomRequest, playerId);
        assertNotNull(roomService.getRoomById(roomId));
    }

    @Test(expected = GameRuleException.class)
    public void getRoomByIdFail() throws IOException, GameRuleException {
        assertNull(roomService.getRoomById("3"));
    }

    @Test
    public void createRoomSuccess() throws IOException, GameRuleException {
        String roomId = roomService.createRoom(createRoomRequest, playerId);
        assertEquals("44", roomService.getRoomById(roomId).getPlayers().get(0).get("playerId"));
    }


}
