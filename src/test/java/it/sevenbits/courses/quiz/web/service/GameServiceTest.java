package it.sevenbits.courses.quiz.web.service;


import it.sevenbits.courses.quiz.core.exception.GameProhibitedException;
import it.sevenbits.courses.quiz.core.exception.GameRuleException;
import it.sevenbits.courses.quiz.core.model.*;
import it.sevenbits.courses.quiz.core.repository.*;
import it.sevenbits.courses.quiz.web.model.AnswerQuestionRequest;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

public class GameServiceTest {
    private GameService gameService;
    private SimpleGameRepository gameRepository;
    private IRoomRepository roomRepository;
    private IQuestionRepository questionRepository;
    private QuestionWithOptions questionWithOptions;
    private QuestionWithOptionsAndCorrectAnswer questionWithOptionsAndCorrectAnswer;
    String playerId;

    private String roomId;

    @Before
    public void setUp(){
        playerId = "1";
        roomId = "1";
        gameRepository = new SimpleGameRepository();
        roomRepository = mock(IRoomRepository.class);
        Room room = mock(Room.class);
        List<Map<String, String>> players = new ArrayList<>();
        Map<String, String> player = new HashMap<>();
        player.put("playerId", playerId);
        players.add(player);
        when(room.getPlayers()).thenReturn(players);
        when(roomRepository.getRoomById(roomId)).thenReturn(room);
        questionRepository = mock(IQuestionRepository.class);
        String questionText = "test";
        List<QuestionAnswer> answers = new ArrayList<>();
        QuestionAnswer correctAnswer = mock(QuestionAnswer.class);
        when(correctAnswer.getAnswerId()).thenReturn("4");
        answers.add(correctAnswer);
        questionWithOptionsAndCorrectAnswer =
                new QuestionWithOptionsAndCorrectAnswer(questionText, answers, correctAnswer);
        questionWithOptions =
                new QuestionWithOptions(questionWithOptionsAndCorrectAnswer.getQuestionId(), questionText, answers);
        List<String> ids = new ArrayList<>();
        ids.add(questionWithOptions.getQuestionId());
        when(questionRepository.getQuestionsIds()).thenReturn(ids);
        when(questionRepository.getFullQuestion(anyString())).thenReturn(questionWithOptionsAndCorrectAnswer);
    }

    @Test
    public void getNextQuestionIdTestSuccess() throws GameRuleException, GameProhibitedException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        assertEquals(questionWithOptions.getQuestionId(), gameService.getNextQuestionId(roomId, playerId));
    }

    @Test(expected = GameRuleException.class)
    public void getNextQuestionIdTestFail() throws GameRuleException, GameProhibitedException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        gameService.getNextQuestionId(roomId, playerId);
        gameService.getNextQuestionId(roomId, playerId);
    }

    @Test
    public void getNextQuestionSuccess() throws GameRuleException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        assertEquals(questionWithOptions, gameService.getNextQuestion(roomId, questionWithOptions.getQuestionId()));
    }

    @Test
    public void checkAnswer() throws GameRuleException, GameProhibitedException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        String questionId = gameService.getNextQuestionId(roomId, playerId);
        gameService.getNextQuestion(roomId, questionId);
        AnswerQuestionRequest answerQuestionRequest =
                new AnswerQuestionRequest(questionWithOptionsAndCorrectAnswer.getCorrectAnswer().getAnswerId());
        assertEquals(questionWithOptionsAndCorrectAnswer.getCorrectAnswer().getAnswerId(),
                gameService.checkAnswer(roomId, questionId, answerQuestionRequest, playerId).getAnswerId());
    }

    @Test(expected = GameRuleException.class)
    public void checkAnswerInappropriateAnswer() throws GameRuleException, GameProhibitedException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        String questionId = gameService.getNextQuestionId(roomId, playerId);
        gameService.getNextQuestion(roomId, questionId);
        AnswerQuestionRequest answerQuestionRequest =
                new AnswerQuestionRequest("5");
        assertEquals(questionWithOptionsAndCorrectAnswer.getCorrectAnswer().getAnswerId(),
                gameService.checkAnswer(roomId, questionId, answerQuestionRequest, playerId).getAnswerId());
    }
    @Test
    public void updateScoreSuccess() throws GameRuleException, GameProhibitedException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        Integer previousGameScore = gameService.getGameScore(roomId);
        String questionId = gameService.getNextQuestionId(roomId, playerId);
        gameService.getNextQuestion(roomId, questionId);
        AnswerQuestionRequest answerQuestionRequest =
                new AnswerQuestionRequest(questionWithOptionsAndCorrectAnswer.getCorrectAnswer().getAnswerId());
        gameService.checkAnswer(roomId, questionWithOptions.getQuestionId(), answerQuestionRequest, playerId);
        assertEquals((Integer)(previousGameScore + 1), gameService.getGameScore(roomId));
    }

    @Test
    public void getGameStatusStarted() throws GameRuleException, GameProhibitedException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        Integer previousGameScore = gameService.getGameScore(roomId);
        String questionId = gameService.getNextQuestionId(roomId, playerId);
        assertEquals("Started", gameService.getGameStatus(roomId).getStatus());
    }

    @Test
    public void getGameStatusNotStartedYet() throws GameRuleException {
        gameService = new GameService(questionRepository, gameRepository, roomRepository);
        assertEquals("Waiting to start", gameService.getGameStatus(roomId).getStatus());
    }

}
